# ctjdfs

#### 介绍
 ctjdfs是由java语言开发的一个开源的轻量级分布式文件系统，它对文件进行管理，功能包括：文件存储、文件访问（文件上传、文件下载）等，解决了大容量存储和负载均衡的问题。

#### 官方网站

项目主页：http://www.caitaojun.com/ccblog/ArticleInfoServlet?aid=003fee02581e4f1a8a368b1e2dfd04ce

#### 软件架构
![](http://res.caitaojun.com/ccblog/image/003fee02581e4f1a8a368b1e2dfd04ce/3f688fc9695641fa9855e388d322c614.png)

#### 安装教程

1. 安装jre1.8环境
2. 服务端下载地址：<http://res.caitaojun.com/ctjdfs-server.jar>，也可以下载ctjdfs-server代码进行打包。
3. 分别配置一个tracker.properties的配置文件和一个storage.properties文件
tracker.properties
```properties
#服务器角色 tracker、storage
ctjdfs.role=tracker
#tracker 2688   storage 2689
ctjdfs.port=2688
#tracker数据文件存放路径
ctjdfs.tracker.data.path=c:/ctjdfs_tracker
#storage文件存储块大小(MB)
ctjdfs.storage.block.size=20
```
storage.properties
```properties
#服务器角色 tracker、storage
ctjdfs.role=storage
#tracker 2688   storage 2689
ctjdfs.port=2689
#tracker 地址、端口
ctjdfs.tracker=127.0.0.1:2688
#storage想tracker发送心跳间隔秒
ctjdfs.heartbeat.interval=10
#storage地址端口
ctjdfs.storage=127.0.0.1:2689
#storage上传文件存储路径
ctjdfs.storage.file.path=c:/ctjdfs
```
4. 启动服务
  上面下载了ctjdfs-server.jar服务程序，以及配置好配置文件后，然后我们分别来运行tracker服务和storage服务。
  首先打开命令窗口，切换到ctjdfs-server.jar所在的目录
  启动tracker服务
  ![](http://res.caitaojun.com/ccblog/image/003fee02581e4f1a8a368b1e2dfd04ce/12fbfd624f8b4828956bab05ec717ca8.png)
  启动storage服务
  ![](http://res.caitaojun.com/ccblog/image/003fee02581e4f1a8a368b1e2dfd04ce/4d22de75ef4f4ebf98dd48b2688c5e17.png)

####客户端应用程序调用

​	客户端应用程序需要导入ctjdfs-client的jar包，maven坐标如下：

```xml
<dependency>
    <groupId>com.caitaojun.ctjdfs</groupId>
    <artifactId>ctjdfs-client</artifactId>
    <version>1.0</version>
</dependency>
```

​	通过代码来实现对文件的上传、下载、删除，代码 如下：

```java
@Test
public void testUpload() throws Exception {
    CtjDfsClient.init("127.0.0.1",2688);
    File file = new File("c:/dog.jpg");
    Map metaData = new HashMap<>();
    metaData.put("author","ctj");
    String fileId = CtjDfsClient.upload(file, metaData);
    System.out.println(fileId);
}

@Test
public void testDownload() throws Exception {
    CtjDfsClient.init("127.0.0.1",2688);
    String fileid = "c7637c97eed3bde8b64dd321c917aa89";
    DownloadFile downloadFile = CtjDfsClient.download(fileid);
    String fileName = downloadFile.getStorageFileInfo().getName();
    FileInputStream fileInputStream = downloadFile.getFileInputStream();
    FileUtils.copyInputStreamToFile(fileInputStream,new File("c:/"+fileName));

}

@Test
public void testDelete() throws Exception {
    CtjDfsClient.init("127.0.0.1",2688);
    String fileid = "904d7ab95fe603eeabb45640a2cb6427";
    boolean delete = CtjDfsClient.delete(fileid);
    System.out.println(delete);
}
```



#### 欢迎各位大神一起能把这个系统搞好起来，后期代码我也继续进行优化更新中。

打赏赞助：http://www.caitaojun.com/ccblog/sponsor.html

讨论qq群：646224436 

